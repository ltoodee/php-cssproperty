<?PHP
session_start();

if(isset($_GET["clear"]))
{
	session_destroy();
	session_regenerate_id();
}

$history = isset($_SESSION["history"]) ? $_SESSION["history"] : array();
$current = isset($_SESSION["current"]) ? $_SESSION["current"] : "";
$prop = $_POST["property"];
$val = $_POST["value"];

if(isset($prop, $val))
{
	$current .= $prop . ":" . $val . ";";
	
	$history[$prop] = $val;
}
?>
<html>
<head>
<style type="text/css">
* {
	font-family: 'Comic Sans MS'
}

body {
	font-size: 12px;
}

.ui-menu-item {
	font-size: 0.8em;	
}

div.border {
	border: 1px solid #4285FA;
	float: left;
	padding: 4px;
	background-color: #D6E4FD;
}

div.subject {
	width: 300px;
	height: 300px;
	background-color: #4285F4;
	padding: 12px;
}

div.subject2 {
	font-weight: bold;
}
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
</head>
<body>
<h1>CSS property tester</h1>

<form method="post" action="#">
	<input type="text" name="property" placeholder="Property" ID="property" /> : 
	<input type="text" name="value" placeholder="Value" />
	<input type="submit" value="Let's see!" />
</form>

<div class="border">
	<div class="subject subject2" style="<?php print $current; ?>">
	<?php
	if(isset($history) && count($history) > 0)
	{
		print "Properties added:<br /><br />";
		foreach($history as $key => $val)
		{
			if(isset($key) && isset($val))
			{
				print "$key : $val<br />";
			}
		}
	}
	?>
	</div>
</div>

<script>
$(function() {
	var availableTags = [
		"accelerator", "azimuth", "background", "background-attachment", "background-color", "background-image", "background-position", "background-position-x", "background-position-y", "background-repeat", "behavior", "border", "border-bottom", "border-bottom-color", "border-bottom-style", "border-bottom-width", "border-collapse", "border-color", "border-left", "border-left-color", "border-left-style", "border-left-width", "border-right", "border-right-color", "border-right-style", "border-right-width", "border-spacing", "border-style", "border-top", "border-top-color", "border-top-style", "border-top-width", "border-width", "bottom", "caption-side", "clear", "clip", "color", "content", "counter-increment", "counter-reset", "cue", "cue-after", "cue-before", "cursor", "direction", "display", "elevation", "empty-cells", "filter", "float", "font", "font-family", "font-size", "font-size-adjust", "font-stretch", "font-style", "font-variant", "font-weight", "height", "ime-mode", "include-source", "layer-background-color", "layer-background-image", "layout-flow", "layout-grid", "layout-grid-char", "layout-grid-char-spacing", "layout-grid-line", "layout-grid-mode", "layout-grid-type", "left", "letter-spacing", "line-break", "line-height", "list-style", "list-style-image", "list-style-position", "list-style-type", "margin", "margin-bottom", "margin-left", "margin-right", "margin-top", "marker-offset", "marks", "max-height", "max-width", "min-height", "min-width", "-moz-binding", "-moz-border-bottom-colors", "-moz-border-left-colors", "-moz-border-radius", "-moz-border-radius-bottomleft", "-moz-border-radius-bottomright", "-moz-border-radius-topleft", "-moz-border-radius-topright", "-moz-border-right-colors", "-moz-border-top-colors", "-moz-opacity", "-moz-outline", "-moz-outline-color", "-moz-outline-style", "-moz-outline-width", "-moz-user-focus", "-moz-user-input", "-moz-user-modify", "-moz-user-select", "orphans", "outline", "outline-color", "outline-style", "outline-width", "overflow", "overflow-X", "overflow-Y", "padding", "padding-bottom", "padding-left", "padding-right", "padding-top", "page", "page-break-after", "page-break-before", "page-break-inside", "pause", "pause-after", "pause-before", "pitch", "pitch-range", "play-during", "position", "quotes", "-replace", "richness", "right", "ruby-align", "ruby-overhang", "ruby-position", "scrollbar-3d-light-color", "scrollbar-arrow-color", "scrollbar-base-color", "scrollbar-dark-shadow-color", "scrollbar-face-color", "scrollbar-highlight-color", "scrollbar-shadow-color", "scrollbar-track-color", "-set-link-source", "size", "speak", "speak-header", "speak-numeral", "speak-punctuation", "speech-rate", "stress", "table-layout", "text-align", "text-align-last", "text-autospace", "text-decoration", "text-indent", "text-justify", "text-kashida-space", "text-overflow", "text-shadow", "text-transform", "text-underline-position", "top", "unicode-bidi", "-use-link-source", "vertical-align", "visibility", "voice-family", "volume", "white-space", "widows", "width", "word-break", "word-spacing", "word-wrap", "writing-mode", "z-index", "zoom"
	];
	
	$( "#property" ).autocomplete({
		source: availableTags
	});
});	
</script>
</body>
</html>
<?php

$_SESSION["history"] = $history;
$_SESSION["current"] = $current;
?>